var schedule = require('node-schedule');
var XMLHttpRequest = require('w3c-xmlhttprequest').XMLHttpRequest;
var xhr = new XMLHttpRequest();
const method = 'X-CMC_PRO_API_KEY';
const key = 'key';


var CryptoNewsAPI = require('crypto-news-api').default;
const Api = new CryptoNewsAPI('key')
const ProxyApi = new CryptoNewsAPI('key', 'http://cryptocontrol_proxy/api/v1/public')

const Sequelize = require('sequelize');
const sequelize = new Sequelize('db_name', 'root', 'password', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000

    }
});
var j = schedule.scheduleJob('*/40 * * * * *', function(){

    xhr.open('GET', 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=20&convert=USD');
    xhr.setRequestHeader(method, key);
    xhr.responseType = 'json';
    xhr.setRequestHeader('Accept', 'application/json');

    xhr.onreadystatechange = function handler() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var repsonse = JSON.parse(xhr.responseText);

            responseUSD(repsonse);
        }
    };

    xhr.send();});

Currencies = sequelize.define('Currencies', {
        id_firs:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        id:{
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING
        },
        symbol: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING
        },

        cmc_rank: {
            type: Sequelize.INTEGER
        },
        price:{
            type:Sequelize.DOUBLE
        },
        volume_24h:{
            type:Sequelize.DOUBLE
        },
        percent_change_1h:{
            type:Sequelize.DOUBLE

        },
        percent_change_24h:{
            type:Sequelize.DOUBLE

        },
        percent_change_7d:{
            type:Sequelize.DOUBLE

        }

    }
);

function responseUSD(response) {
    response.data.forEach(function (val) {
        var quote = val.quote;
        for(var property in quote){
            if(quote.hasOwnProperty(property)){
                Currencies.sync().then(function () {

                    createOrUpdate({
                            id: val.id,
                            name: val.name,
                            symbol: val.symbol,
                            slug: val.slug,
                            cmc_rank: val.cmc_rank,
                            price: quote[property].price,
                            volume_24h: quote[property].volume_24h,
                            percent_change_1h: quote[property].percent_change_1h,
                            percent_change_24h: quote[property].percent_change_24h,
                            percent_change_7d: quote[property].percent_change_7d },
                        {name: val.name})

                })}
        }})}






function createOrUpdate(values, name){
    return Currencies.findOne({where:name}).then(function (curObj) {
        if(curObj){
            return Currencies.update(values, {where: name})
        }
        else {
            return Currencies.create(values);
        }

    })
}

News = sequelize.define('news', {
        id_firs:{
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        id:{
            type: Sequelize.STRING
        },
        description:{
            type:Sequelize.TEXT
        },
        soursUrl:{
            type: Sequelize.STRING
        },
        name:{
            type: Sequelize.STRING
        },
        category:{
            type: Sequelize.STRING
        },
        title:{
            type: Sequelize.STRING
        },

        url:{
            type: Sequelize.STRING
        },
        publishedAt:{
            type: Sequelize.DATE
        },
        originalImageUrl:{
            type: Sequelize.STRING
        }







    }
);

var rule = new schedule.RecurrenceRule();

rule.minute = new schedule.Range(0, 59, 5);
schedule.scheduleJob(rule, function() {
    Api.getTopNews()
        .then(function (articles) {
            (articles.forEach(function (element) {
                News.sync().then(function () {
                    createOrUpdateNews({

                        id: element._id,
                        description: element.description,
                        soursUrl: element.source.url,
                        name: element.source.name,
                        category: element.primaryCategory,
                        title: element.title,

                        url: element.url,
                        publishedAt: element.publishedAt,
                        originalImageUrl: element.originalImageUrl
                    }, {id: element._id})
                })
            }))
        })
        .catch(function (error) {
            console.log(error)
        });
});


function createOrUpdateNews(values, id){
    return News.findOne({where: id}).then(function (obj) {
        if(obj){
            return News.update(values, {where: id})
        }
        else {
            return News.create(values);
        }

    })
}